---
title: "This week in GNOME Builder #1"
date: 2020-07-03
tags: ["gnome builder"]
categories: ["gnome"]
---
Hello! My name is Günther Wagner and i try to give some insights in the current 
development of GNOME Builder. All these changes can already been tested with 
[GNOME Builder Nightly](https://wiki.gnome.org/Apps/Builder) so go ahead and give us feedback! This newsletter is called "This week in ..." but 
probably we won't post every week. So the interval will be a little bit 
arbitrary. Let's start!

## New Features

We included a new code spellchecker plugin leverage the fantastic 
[codespell-project](https://github.com/codespell-project/codespell/).

![codespell](/posts/images/codespell.png)

It is now possible to open files in the Project Tree with any external Program.

![external program](/posts/images/external_program.png)

## Honourable mentions

The Markdown previewer faced some problems with 
[incorrect escapes](https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/1244). 
Several problems got fixed along the way!

Alberto added an [extension point](https://gitlab.gnome.org/GNOME/gnome-builder/-/commit/650d6119d8f94b4d3c9e1be9ebaed730e6ecb9bc) to allow the distribution of out-of-tree plugins via flatpak-extensions. Currently there are no out-of-tree plugins as the model in GNOME Builder is currently similar to the linux-kernel. Assimilation of the plugins allows us to link them statically which improves the startup performance significantly. Christian also enabled the usage of libportal if there would be the need for plugins.

C++ detection got improved so clang is able to provide the correct completions 
and diagnostics.

The highlighting for diagnostic got improved. Before we always marked the element from the beginning of the problem till the end of the line. Now ranges getting considered aswell and mark only the affected range of the problem. If there is only the start point given we mark only the element affected element instead of the whole line. Speaking about diagnostic marker we fixed a bug which only showed the first diagnostic on a line. Now all diagnostics are getting shown in the popover.

**Before**

![diagnostic before](/posts/images/diagnostic_before.png)

**After**

![diagnostic after](/posts/images/diagnostic_after.png)

Diego streamlined and fixed several tooltip positioning problems and visual shortcomings.


**Fixed Issues:**
  * Crash when Builder faced a project outside of home - https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/1246
  * Rust templates used bash only syntax - https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/1254
  * "Git line changed" indicator gets not updated - https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/1248
