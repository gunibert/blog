+++
date = 2019-10-04
title = "Design applications with convergence in mind"
tags = ["gtk", "application development"]
categories = ["gnome"]
+++

![Navigation Hackgregator](hn-phone-1.png)

Developing an application with convergence in mind will be a new space in linux
application development. The upcoming Librem5 from Purism allows application 
developers to run standard applications in a new form factor.

<!-- more -->
They develop a companion library [libhandy](https://gitlab.gnome.org/Community/Purism/libhandy) with Gtk widgets derived from 
standard Gtk to accomplish a convergence between desktop and smartphone usage 
without re-developing everything from scratch. This is really handy 
(pun intended) because most probably you just have to use a different container 
for most things. For example you used a `GtkBox` for a static sidebar in your 
application then you can use `HdyLeaflet` now which acts similar but offers in 
small form factors a different behaviour.

![Fractal convergence](fractal-convergence.png)

To design an application from the scratch its most probably best to think first 
about the small form factor and try later to fit a good desktop usage. I tried 
this with a simple hacker news reader application and my design looked like 
that:

![Hackgregator convergence](hn-convergence.png)

This worked very well and i have a good understanding about the UX now. The 
smartphone usage is already implemented and next i want to make the desktop 
usage better.

![Hackgregotar Final](hn-final.png)

I hope you liked my development logbook. I want to keep up with the development 
of this application and write from time to time a post about the current status.
