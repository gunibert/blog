---
title: "Ported GNOME Mahjongg to Gtk4 - my journey"
date: 2021-11-26
tags: ["gnome mahjongg"]
categories: ["gnome"]
---

Everything started with a tweet by Alexander Mikhaylenko about the state of the "Dark Style Preference" Initiative. As i play Mahjongg from time to time i wanted to help with the Initiative and postulated the goal "Lets make GNOME Mahjongg dark stylable". After digging a little bit in the code i tried to directly port to Gtk4. I mean, how hard can it be. Its just a small game with a **GtkDrawingArea**. (spoiler it was harder then i thought)

![mahjongg-gtk3.png](mahjongg-gtk3.png)

## Deprecations
Some rendering code for the tiles was using deprecated methods. These are gone in Gtk4 therefore i first had to fix this. Additionally i had to change:

* `GtkGestureMultiPress` is now called `GtkGestureClick`
* `queue_draw_area` is no more
* many changes to `GtkBox`. There are no `pack_start` or `pack_end` methods are available anymore. This is significant as its crucial in which order elements are getting added to boxes.
* all `GtkDialog` elements are used in their blocking variant and i had to make them asynchronous.

## Little refactorings
As the code was really cluttered with many GUI related construction code i decided to introduce composite templates into the project as this will significantly reduce the code and makes it more maintainable in the future. Lesson i learned doing this was, that its perfectly doable to port dialogs to composite templates but its important to stick to the default layout. Adding widgetry as child without the type `internal-child="content_area"` will damage the function of a typical dialog and you won't get action widgets correctly.

![gnome-mahjongg-gtk4.png](gnome-mahjongg-gtk4.png)

## Bringing in libadwaita
As the dark style preference (my initial goal) will be automatically used when i use libadwaita i decided to use this from now on. Libadwaita has some niceties which made the work really a pleasure:

* if you have a style.css as a resource in your project directly in the resource-base-path then this will be automatically loaded and applied. No more CSS loading and applying manually. Same for CSS rules for dark mode (style-dark.css in that case)
* The `AdwHeaderBar` together with `AdwWindowTitle` made it easy to bring a title and subtitle implementation into the headerbar.
* The AdwPreferences implementation is really nice and made it easy to implement

![gnome-mahjongg-gtk3-pref.png](gnome-mahjongg-gtk3-pref.png)
![gnome-mahjongg-gtk4-pref.png](gnome-mahjongg-gtk4-pref.png)

## Closing
Overall i like where Gtk4 and libadwaita is heading to. Functionality is improved on several things and the API is now better then before. GNOME 42 will be an awesome release! Stay tuned