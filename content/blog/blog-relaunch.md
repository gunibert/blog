---
title: "Blog relaunch"
date: 2022-11-03
---

Probably the bigger part the dev community experiments from time to time with a blog. Writing blog posts is an ability worth to reach for and only practicing it makes us better. But also there is the wish to deploy this with a machinery fitting to our dev style. Some are using simple [wordpress](https://wordpress.com/) or [ghost](https://ghost.org/) (or even hosted solutions like [Medium](https://medium.com/)) to write their blogs. Then we have the faction "static is fine" which generates all the static files for a blog from a markdown repository. I experimented with both and settled for now on [zola](https://www.getzola.org/).

## Why zola?

I settled for zola because its super versatile. I can have my portfolio and my blog combined in a single repository and can alter the complete website with a bunch of tera templates. Starting with zola is a little bit complicated as it is not meant for blogging alone but also for static websites promoting an association or non-profit (which don't change that often). I will describe the zola way in a future post.

## Future plans

In general i try to blog now more often. I setup everything so i don't have a big maintenance burden in the future. My CI/CD deployment pipeline will automatically push updates from the repository to my webserver so everything should be fine. This gives room for writing and that is exactly what i plan for now.

## An SPA written in Rust?

On the other hand i can develop an identity for my blog. If i have a style i am pleased with i can reuse it for whatever comes next. I am really eager to have instead a static application a yew/sycamore SPA instead. Not because it will bring any additional benefits but i would love to develop such a system in the future just for fun 😄️. To be honest i already started with it 🥳️.