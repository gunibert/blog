---
title: "This week in GNOME Builder #2"
date: 2020-07-11
tags: ["gnome builder"]
categories: ["gnome"]
draft: false
---
This week we fixed some specific topics which were planned for the previous 
cycle. If anyone wants to contribute so see some of our "Builder wishlist" go
 there: [Builder/ThreePointThirtyfive](https://wiki.gnome.org/Apps/Builder/ThreePointThirtyfive)
    
Last time i had forgotten to mention the great work of our translation team 
which contributed various translations to Builder. Thank you!
    
## New Features

For several releases now Builder allows to debug applications with `gdb`. However it
was not possible to interact with `gdb` directly. This shortcoming got fixed now and there
is now an entry to execute commands against the `gdb` process.

![Debugger Console](/posts/images/Debugger_Console.png)

We are happy if you can test this feature to iron most of the problems out before
the release this cycle.

## Plugins

Recently [**podman**](https://podman.io) got major changes and announced V2 of
the project. This changed also some APIs from the tool which broke our podman
plugin. Harry to the rescue: he [fixed](https://gitlab.gnome.org/GNOME/gnome-builder/-/commit/0f32d1f4a6e64e37894a338b9cc75b3f0b02528d)
the plugin and made it even backwards compatible. So you can use still the older
version.

The **Rust Analyzer** Plugin got an alternative algorithm for resolving the
actual workspace. Most of the time this would be the open project (as `Cargo.toml`
can be found on the root of the project). But in hybrid projects where you
maybe try to RiiR the `Cargo.toml` file can be found in a deeper folder. The
idea now is to search for the Cargo-file in the root of the open project and if
there is no file found trying to start the search from the opened Rust-file
upwards till we reach the root folder again.

Christian tweaked some colours for the gutter for the new upcoming Adwaita
colortheme (work in progress!)

## Issues

* Vanadiae fix the CSS to proper render shadows for attention dots - [MR-282](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/282)
* Matthew [fixed](https://gitlab.gnome.org/GNOME/gnome-builder/-/commit/467780a063acf094dc16639bc8ed1ee4da191378) the modeline in modeline-parser.c
