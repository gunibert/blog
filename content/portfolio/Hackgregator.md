---
date: 2020-06-22
title: "Hackgregator"
description: "A hackernews reader application for the GNOME desktop"
author: "Günther Wagner"
tags: ["gnome"]
---

Hackgregator is a neat little reading application designed for GNOME desktop
environment. With the launch of the [Librem 5](https://puri.sm/products/librem-5/)
i asked myself what i would do with a Linux powered GNOME phone. As i mostly
use my smartphone to bridge phases of boredness i thought reading Hacker News
as i do with my Android powered phone would be a good and needed application.

* [Gitlab](https://gitlab.com/gunibert/hackgregator)
* Get it on [Flathub](https://flathub.org/apps/details/de.gunibert.Hackgregator)

![Hackgregator Landing](https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-landing.png)

Hackgregator can be used as a desktop application and a mobile application. I
use a library called [libhandy](https://gitlab.gnome.org/GNOME/libhandy) which
enables this convergence use-case.

![Hackgregator Mobile](https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-mobile.png)

