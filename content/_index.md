---
---

Hi! I'm Günther Wagner. I blog from time to time about technical things in the GNOME universe or about Rust. Currently i am working as a backend engineer at [Luminovo GmbH](https://luminovo.ai/).

#### Skills:

* Rust
* C
* Python
* DevOps